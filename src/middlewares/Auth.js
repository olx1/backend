const User = require('../models/User');

module.exports = {
  private: async (req, res, next) => {
    if (!req.body.token) {
      res.json({ error: 'Token não informado' });
      return;
    }

    const { token } = req.body;

    if (token === '') {
      res.json({ notallowed: true });
      return;
    }
    const user = await User.findOne({
      token,
    });

    console.log(user);

    if (!user) {
      res.json({ notallowed: true });
      return;
    }

    next();
  },
};
